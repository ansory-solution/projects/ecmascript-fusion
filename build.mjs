import * as babel from "@babel/core"
import browserify from "browserify"
import * as fs from "node:fs"
import * as fsp from "node:fs/promises"
import * as path from "node:path"

import * as Config from "./config.mjs"

await fsp.mkdir("./build", { recursive: true });
const files = [Config.Main, ...Config.Files]
	.map(file => ({
		dir: path.dirname(file),
		ext: path.extname(file),
		name: file
	})).map(file => ({
		...file,
		name: path.basename(file.name, file.ext)
	}));

for (const file of files) {
	const result = await babel.transformFileAsync(path.join(".", file.dir, `${file.name}${file.ext}`), Config.Babel);
	await fsp.mkdir(path.join(".", "build", file.dir), { recursive: true });
	await fsp.writeFile(path.join(".", "build", file.dir, `${file.name}.js`), result.code);
}

const format = browserify(path.join(".", "build", files[0].dir, `${files[0].name}.js`));
await fsp.mkdir("./out", { recursive: true });
format.plugin("tinyify").bundle().pipe(fs.createWriteStream(`./out/${files[0].name}.js`));
