
export const Main = "main.ts"

export const Files = [
	"src/test.ts"
];

export const Babel = {
    "presets": [
		[
			"@babel/preset-env",
			{
				"targets": {
					"chrome": "46"
				},
				"useBuiltIns": "usage",
				"corejs": "3.32.0"
			}
		],
        "@babel/preset-typescript"
	]
};
