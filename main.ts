
import { Result, test } from "./src/test";

async function main(): Promise<Result> {
	const wait = test();
	console.log("main");
	await wait;
    return 0;
}

main().then(result => {
    console.log(result);
});
