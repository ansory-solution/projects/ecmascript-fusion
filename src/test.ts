
export type Result = number;

export function sleep(ms: number) {
	return new Promise(resolve => {
		setTimeout(resolve, ms);
	});
}

export async function test() {
	const list = [1, 2, 3, 4, 5];
	for (const num of list) {
		console.log(`${num}: Begin`);
		await sleep(num * 100);
		console.log(`${num}: End`);
	}
	console.log("Done");
}
